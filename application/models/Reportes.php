<?php
class Reportes extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * @param bool $slug
	 * @return mixed
	 * Busca reportes
	 */
	public function getReportes($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('reporte_ingreso_egreso');
			return $query->result_array();
		}

		$query = $this->db->get_where('reporte_ingreso_egreso', array('id_usuario' => $slug));
		return $query->row_array();
	}

	/**
	 * @return mixed
	 * consigue todos los datos de reportes y relaciones
	 */
	public function getAll()
	{
		$this->db->select('*');
		$this->db->from('reporte_ingreso_egreso','usuarios','turnos_usuarios','horarios_usuarios');
		$this->db->join('usuarios', 'usuarios.id = reporte_ingreso_egreso.id_usuario','inner');
		$this->db->join('turnos_usuarios', 'turnos_usuarios.id_usuario =  reporte_ingreso_egreso.id_usuario', 'inner');
		$this->db->join('horarios_usuarios', 'horarios_usuarios.id_usuario =  reporte_ingreso_egreso.id_usuario', 'inner');
		$this->db->order_by('usuarios.nombres', 'ASC');
		$this->db->order_by('turnos_usuarios.order', 'ASC');
		$this->db->order_by('horarios_usuarios.fecha',  'DESC');
		$this->db->distinct();
		$query = $this->db->get();
		return $query->result_array();
	}
}
